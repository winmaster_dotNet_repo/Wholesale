﻿using System.Windows.Forms;
using HurtSys.Controler;
using HurtSys.Model;
using NUnit.Framework;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;


namespace HurtSys_UnitTests
{
    /// <summary>
    /// Klasa testująca przebieg zmimany danych użytkownika
    /// </summary>
    [TestFixture]
    public class ChangeUserDataTest
    {
        private TextBox oldPass;
        private TextBox newPass;
        private TextBox validateNewPass;
        private TextBox name;
        private TextBox surname;
        private TextBox email;
        private TextBox phone;


        /// <summary>
        /// Metoda ustawiająca pewne parametry potrzebne do testów, dane użytkownika
        /// </summary>
        [SetUp]
        public void SetUp()
        {
           oldPass = new TextBox();
           newPass = new TextBox();
           validateNewPass = new TextBox();
           name = new TextBox();
           surname = new TextBox();
           email = new TextBox();
           phone = new TextBox();
           
           AccountManager.hurtSysUser  = HurtSysUser.Instance;
           AccountManager.hurtSysUser.ID = 7;
           AccountManager.hurtSysUser.Login = "krzwik";
           AccountManager.hurtSysUser.Password = "5089459c592abe1e8b0e08f433611ab9865f36106725d0e52aefa89e7ab1ec6e";
           AccountManager.hurtSysUser.UsrEmail = "krzwik@hurtsys.pl";
           AccountManager.hurtSysUser.UsrName = "Krzysztof";
           AccountManager.hurtSysUser.UsrSurname = "Wilk";
           AccountManager.hurtSysUser.UsrPhone = "121212122";
        }

        /// <summary>
        /// Test sprawdzający czy pola zmiany hasła zostały zmienione, podawane wartości puste
        /// </summary>
        [Test, Category("Unit")]
        public void CheckIfPasswordTextBoxChanged_TestNullValue()
        {
            oldPass.Text = "";
            newPass.Text = "";
            validateNewPass.Text = "";
            bool result = AboutUserController.CheckIfPasswordTextBoxChanged(oldPass, newPass, validateNewPass);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        ///  Test sprawdzający czy pola zmiany hasła zostały zmienione, podawana jedna wartość
        /// </summary>
        [Test, Category("Unit")]
        public void CheckIfPasswordTextBoxChanged_TestOneFullValue()
        {
            oldPass.Text = "xxx";
            newPass.Text = "";
            validateNewPass.Text = "";
            bool result = AboutUserController.CheckIfPasswordTextBoxChanged(oldPass, newPass, validateNewPass);
            Assert.AreEqual(true, result);
        }

        /// <summary>
        ///  Test sprawdzający czy pola zmiany hasła zostały zmienione, dwie zmienione wartości, jedna pusta
        /// </summary>
        [Test, Category("Unit")]
        public void CheckIfPasswordTextBoxChanged_TestOneNullValue()
        {
            oldPass.Text = "xxx";
            newPass.Text = "";
            validateNewPass.Text = "xxx";
            bool result = AboutUserController.CheckIfPasswordTextBoxChanged(oldPass, newPass, validateNewPass);
            Assert.AreEqual(true, result);
        }

        /// <summary>
        ///  Test sprawdzający czy pola zmiany hasła zostały zmienione, wszystkie zmienione
        /// </summary>
        [Test, Category("Unit")]
        public void CheckIfPasswordTextBoxChanged_TestNoNullValues()
        {
            oldPass.Text = "xxx";
            newPass.Text = "xxx";
            validateNewPass.Text = "xxx";
            bool result = AboutUserController.CheckIfPasswordTextBoxChanged(oldPass, newPass, validateNewPass);
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność wszystkich składowych zmiany hasła, test pozytywny
        /// </summary>
        [Test, Category("Unit")]
        public void ValidatePasswordChange_TestPositive()
        {
            oldPass.Text = "groupware";
            newPass.Text = "testyJednostkowe";
            validateNewPass.Text = "testyJednostkowe";

            bool result = AboutUserController.ValidatePasswordChange(oldPass, newPass, validateNewPass, false);
            Assert.AreEqual(true,result);
        }

        /// <summary>
        /// Test sprawdzający poprawność wszystkich składowych zmiany hasła, hasło obecne błędne
        /// </summary>
        [Test, Category("Unit")]
        public void ValidatePasswordChange_TestPasswordDontMatch()
        {
            oldPass.Text = "group";
            newPass.Text = "testyJednostkowe";
            validateNewPass.Text = "testyJednostkowe";

            bool result = AboutUserController.ValidatePasswordChange(oldPass, newPass, validateNewPass, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność wszystkich składowych zmiany hasła, nowe hasła niespójne
        /// </summary>
        [Test, Category("Unit")]
        public void ValidatePasswordChange_TestNewPasswordsDontMatch()
        {
            oldPass.Text = "groupware";
            newPass.Text = "testyJednostkowe";
            validateNewPass.Text = "testyJed";

            bool result = AboutUserController.ValidatePasswordChange(oldPass, newPass, validateNewPass, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność wszystkich składowych zmiany hasła, nowe hasło za krótkie
        /// </summary>
        [Test, Category("Unit")]
        public void ValidatePasswordChange_TestNewPasswordsTooShort()
        {
            oldPass.Text = "groupware";
            newPass.Text = "testy";
            validateNewPass.Text = "testy";

            bool result = AboutUserController.ValidatePasswordChange(oldPass, newPass, validateNewPass, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający czy pola zmiany danych osobowych zostały zmienione, podawane wartości puste
        /// </summary>
        [Test, Category("Unit")]
        public void  CheckIfUserPersonalDataChanged_TestNullValue()
        {
            name.Text = "";
            surname.Text = "";
            email.Text = "";
            phone.Text = "";

            bool result = AboutUserController.CheckIfUserPersonalDataChanged(name, surname, email, phone);
            Assert.AreEqual(true,result);
        }

        /// <summary>
        /// Test sprawdzający czy pola zmiany danych osobowych zostały zmienione, dane nie zostały zmienione
        /// </summary>
        [Test, Category("Unit")]
        public void CheckIfUserPersonalDataChanged_TestDataDontChange()
        {
            name.Text = "Krzysztof";
            surname.Text = "Wilk";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "121212122";

            bool result = AboutUserController.CheckIfUserPersonalDataChanged(name, surname, email, phone);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający czy pola zmiany danych osobowych zostały zmienione, jedna wartość zmieniona
        /// </summary>
        [Test, Category("Unit")]
        public void CheckIfUserPersonalDataChanged_TestOneChanged()
        {
            name.Text = "Krzysztof";
            surname.Text = "Wilk";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "698022237";

            bool result = AboutUserController.CheckIfUserPersonalDataChanged(name, surname, email, phone);
            Assert.AreEqual(true,result);
        }

        /// <summary>
        /// Test sprawdzający walidację adresu email, mail pusty
        /// </summary>
        [Test, Category("Unit")]
        public void MailIsValid_TestMailEmpty()
        {
            string email = "";
            bool result = BaseController.MailIsValid(email);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający walidację adresu email, email niepoprawny
        /// </summary>
        [Test, Category("Unit")]
        public void MailIsValid_TestMailInvalid()
        {
            string email = "12345ff";
            bool result = BaseController.MailIsValid(email);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający walidację adresu email, mail bez @
        /// </summary>
        [Test, Category("Unit")]
        public void MailIsValid_TestMailWithoutAt()
        {
            string email = "bialas.katarzyna1996.gmail.com";
            bool result = BaseController.MailIsValid(email);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający walidację adresu email, email poprawny
        /// </summary>
        [Test, Category("Unit")]
        public void MailIsValid_TestMailOk()
        {
            string email = "bialas.katarzyna1996@gmail.com";
            bool result = BaseController.MailIsValid(email);
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, test pozytywny
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestPass()
        {
            name.Text = "Krzysztof";
            surname.Text = "Wilkiewicz";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "698022237";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(true, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, dane niezmienione
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestDataDontChange()
        {
            name.Text = "Krzysztof";
            surname.Text = "Wilk";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "121212122";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, Imię puste
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestNameEmpty()
        {
            name.Text = "";
            surname.Text = "Wilk";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "121212122";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, imię z małej litery
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestNameLower()
        {
            name.Text = "krzysztof";
            surname.Text = "Wilk";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "121212122";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, nazwisko puste
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestSurnameEmpty()
        {
            name.Text = "Krzysztof";
            surname.Text = "";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "121212122";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, nazwisko z małej litery
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestSurnameLower()
        {
            name.Text = "Krzysztof";
            surname.Text = "wilk";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "121212122";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, email niepoprawny
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestEmailInvalid()
        {
            name.Text = "Krzysztof";
            surname.Text = "Wilk";
            email.Text = "kblahblah";
            phone.Text = "121212122";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, email bez domeny hurtsys
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestEmailWithoutDomain()
        {
            name.Text = "Krzysztof";
            surname.Text = "Wilk";
            email.Text = "krzwik@onet.pl";
            phone.Text = "121212122";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(false, result);
        }

        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, za króki numer telefonu
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestPhoneTooShort()
        {
            name.Text = "Krzysztof";
            surname.Text = "Wilk";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "12";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(false, result);
        }
        
        /// <summary>
        /// Test sprawdzający poprawność składowych zmiany danych użytkownika, niepoprawny numer telefonu
        /// </summary>
        [Test, Category("Unit")]
        public void ValidateUserPersonalInformation_TestPhoneInvalid()
        {
            name.Text = "Krzysztof";
            surname.Text = "Wilk";
            email.Text = "krzwik@hurtsys.pl";
            phone.Text = "662testy_jednostkowe+48";

            bool result = AboutUserController.ValidateUserPersonalInformation(name, surname, email, phone, false);
            Assert.AreEqual(false, result);
        }

    }
}
