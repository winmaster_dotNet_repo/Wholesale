﻿using System;
using System.Windows.Forms;
using HurtSys.Controler;
using HurtSys.Model;

namespace HurtSys
{
    /// <summary>
    /// Formularz startowy Systemu HurtSys
    /// </summary>
    public partial class FormHome : Form
    {
        private void buttonAbout_Click(object sender, EventArgs e)
        {
            FormAboutUser formAboutUser = new FormAboutUser();
            formAboutUser.Show();
        }

        private void buttonDelivers_Click(object sender, EventArgs e)
        {
            FormDelivers formDelivers = new FormDelivers();
            formDelivers.Show();
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonNewDelivery_Click(object sender, EventArgs e)
        {
            FormDelivery formDelivery = new FormDelivery();
            formDelivery.Show();
        }

        private void buttonRateDelivery_Click(object sender, EventArgs e)
        {
            //FormRateDelivery formRateDelivery = new FormRateDelivery();
            // formRateDelivery.Show();
            BaseController.DefaultMessage();
        }

        /// <summary>
        /// Konstruktor inicjujący formularz startowy
        /// Ustawianie danych użytkownika
        /// Ustawianie tekstu newslettera
        /// Ustawianie listy potrzeb
        /// </summary>
        public FormHome()
        {
            InitializeComponent();
            AccountManager.SetupUserData(labelName, pictureBoxPhoto);
            HomeController.SetNewsletterText(textBoxNewsletter);
            HomeController.SetListOfNeedsHome(dataGridViewListOfNeeds);
        }

        private void dataGridViewListOfNeeds_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            HomeController.ListOfNeedsColorizer(dataGridViewListOfNeeds);
        }

        private void buttonRefreshListOfNeeds_Click(object sender, EventArgs e)
        {
            HomeController.DatagridViewListOfNeedsRefresher(dataGridViewListOfNeeds);
        }
    }
}