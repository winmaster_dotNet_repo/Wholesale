﻿using System;
using System.Windows.Forms;
using HurtSys.Controler;
using HurtSys.Model;

namespace HurtSys
{
    /// <summary>
    /// Formularz do zarządzania dostawcami
    /// </summary>
    public partial class FormDelivers : Form
    {
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDeleteDeliver_Click(object sender, EventArgs e)
        {
            BaseController.DefaultMessage();
        }

        private void buttonEditDeliver_Click(object sender, EventArgs e)
        {
            BaseController.DefaultMessage();
        }

        private void buttonNewDeliver_Click(object sender, EventArgs e)
        {
            BaseController.DefaultMessage();
        }

        /// <summary>
        /// Konstruktor inicjujący formularz dostawców
        /// </summary>
        public FormDelivers()
        {
            InitializeComponent();
            AccountManager.SetupUserData(labelName, pictureBoxPhoto);
            DeliversController.getAllDelivers(dataGridViewDelivers);
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}