﻿using System;
using System.Windows.Forms;
using HurtSys.Model;

namespace HurtSys
{
    /// <summary>
    /// Formularz oceny dostawy
    /// </summary>
    public partial class FormRateDelivery : Form
    {
        /// <summary>
        /// Konstruktor inicjalizujący formularz oceny dostawy
        /// </summary>
        public FormRateDelivery()
        {
            InitializeComponent();
            AccountManager.SetupUserData(labelName, pictureBoxPhoto);
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
