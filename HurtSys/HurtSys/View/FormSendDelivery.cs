﻿using System;
using System.Windows.Forms;
using HurtSys.Controler;

namespace HurtSys.View
{
    /// <summary>
    /// Formularz wysyłki zamówienia do dostawców
    /// </summary>
    public partial class FormSendDelivery : Form
    {
        private static Form lastForm;

        /// <summary>
        /// Konstruktor inicjujący formularz wysyłki zamówienia
        /// </summary>
        /// <param name="form">Formularz wywołujący</param>
        public FormSendDelivery(Form form)
        {
            lastForm = form;
            InitializeComponent();
            SendDeliveryController.StartBackgroundWork(progressBarSendDelivery,timer,backgroundWorker);
            timer.Start();
        }

        private void pictureBoxExtras_Click(object sender, EventArgs e)
        {
            //this.Close();
        }

        private void timer_Tick_1(object sender, EventArgs e)
        {
            if (progressBarSendDelivery.Value >= 100)
            {
                timer.Stop();
                if (SendDeliveryController.SendDelivery(progressBarSendDelivery, pictureBoxExtras))
                {
                    this.Close();
                    lastForm.Close();
                }
            }
            else
            {
                progressBarSendDelivery.Value += 10;
            }
            this.Text = progressBarSendDelivery.Value + " %";
        }

        private void FormSendDelivery_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer.Stop();
        }
    }
}
