﻿using System;
using System.Windows.Forms;
using HurtSys.Controler;
using HurtSys.Model;
using HurtSys.Resources;

namespace HurtSys
{
    /// <summary>
    /// Widok Pokazujący dane użytkownika i umożliwiający ich zmianę
    /// </summary>
    public partial class FormAboutUser : Form
    {
        private void buttonBack_Click(object sender, EventArgs e)
        {
            //sprawdzanie czy nie ma zmienionych danych
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            if (!AboutUserController.CheckIfUserPersonalDataChanged(textBoxName, textBoxSurname, textBoxEmail, textBoxPhone) 
                && !AboutUserController.CheckIfPasswordTextBoxChanged(textBoxOldPass, textBoxNewPass, textBoxVerifyPass))
            {
                ChangeVisibilityOfData(false);
                this.Close();
            }
            else
            {
                result = MessageBox.Show(MessageDictionary.CancelEdition, MessageDictionary.DataNotChanged, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    ChangeVisibilityOfData(false);
                    this.Close();
                }
            }
        }

        private void buttonChangeData_Click(object sender, EventArgs e)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            if (!AboutUserController.CheckIfPasswordTextBoxChanged(textBoxOldPass, textBoxNewPass, textBoxVerifyPass))
            {
                groupBoxUserData.Visible = true;
                groupBoxPassword.Visible = false;

                ChangeVisibilityOfData(true);
            }
            else
            {
                result = MessageBox.Show(MessageDictionary.CancelEdition, MessageDictionary.DataInserted, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    groupBoxUserData.Visible = true;
                    groupBoxPassword.Visible = false;

                    ChangeVisibilityOfData(true);
                }
            }
        }

        private void buttonChangePassword_Click(object sender, EventArgs e)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            
            if (!AboutUserController.CheckIfUserPersonalDataChanged(textBoxName, textBoxSurname, textBoxEmail, textBoxPhone))
            {
                ChangeVisibilityOfData(false);
                groupBoxUserData.Visible = false;
                groupBoxPassword.Visible = true;
            }
            else
            {
                result = MessageBox.Show(MessageDictionary.CancelEdition, MessageDictionary.DataInserted, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    ChangeVisibilityOfData(false);
                    groupBoxUserData.Visible = false;
                    groupBoxPassword.Visible = true;
                }
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            //sprawdzanie czy dane są poprawne
            if (AboutUserController.ValidateUserPersonalInformation(textBoxName, textBoxSurname, textBoxEmail,textBoxPhone))
            {
                DbQueries.UpdateUserPersonalData(textBoxName, textBoxSurname, textBoxEmail, textBoxPhone);
                MessageBox.Show(MessageDictionary.RebootDataChanged);
                Application.Restart();
            }
        }

        private void ChangeVisibilityOfData(bool condition)
        {
            textBoxName.Enabled = condition;
            textBoxSurname.Enabled = condition;
            textBoxEmail.Enabled = condition;
            textBoxPhone.Enabled = condition;
        }

        /// <summary>
        /// Konstruktor Inicjujący formularz danych:
        /// Ustawianie danych użytkownika z bazy danych, zdjęcia użytkownika
        /// </summary>
        public FormAboutUser()
        {
            InitializeComponent();
            AccountManager.SetupUserData(labelName, pictureBoxPhoto);
            AccountManager.SetupUserData(labelName,pictureBoxWorkerPhoto);
            AboutUserController.LoadUserPersonalInformation(textBoxName, textBoxSurname, textBoxEmail, textBoxPhone);
        }


        private void buttonLogout_Click(object sender, EventArgs e)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            //sprawdzanie czy nie ma zmienonych danych
            if (!AboutUserController.CheckIfUserPersonalDataChanged(textBoxName, textBoxSurname, textBoxEmail,textBoxPhone)
                && !AboutUserController.CheckIfPasswordTextBoxChanged(textBoxOldPass, textBoxNewPass, textBoxVerifyPass))
            {
                Application.Exit();
            }
            else
            {
                result = MessageBox.Show(MessageDictionary.ExitAppQuestion, MessageDictionary.DataInserted, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    Application.Exit();
                }
            }
        }

        private void showOldPass(object sender, MouseEventArgs e)
        {
            textBoxOldPass.PasswordChar = '\0';
        }

        private void hideOldPass(object sender, MouseEventArgs e)
        {
            textBoxOldPass.PasswordChar = '*';
        }

        private void showNewPass(object sender, MouseEventArgs e)
        {
            textBoxNewPass.PasswordChar = '\0';
        }

        private void hideNewPass(object sender, MouseEventArgs e)
        {
            textBoxNewPass.PasswordChar = '*';
        }

        private void showValidationPass(object sender, MouseEventArgs e)
        {
            textBoxVerifyPass.PasswordChar = '\0';
        }

        private void hideValidationPass(object sender, MouseEventArgs e)
        {
            textBoxVerifyPass.PasswordChar = '*';
        }

        private void buttonChangePasswordSave_Click(object sender, EventArgs e)
        {
            //sprawdzanie czy dane są poprawne
            if (AboutUserController.ValidatePasswordChange(textBoxOldPass, textBoxNewPass, textBoxVerifyPass))
            {
                DbQueries.UpdateUserPassword(textBoxNewPass);
                MessageBox.Show(MessageDictionary.RebootPasswordChanged);
                Application.Restart();
            }
        }
    }
}