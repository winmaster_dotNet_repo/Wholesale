﻿namespace HurtSys
{
    partial class FormHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxFunctional = new System.Windows.Forms.GroupBox();
            this.dataGridViewListOfNeeds = new System.Windows.Forms.DataGridView();
            this.labelName = new System.Windows.Forms.Label();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.groupBoxNewsletter = new System.Windows.Forms.GroupBox();
            this.textBoxNewsletter = new System.Windows.Forms.TextBox();
            this.panelData = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.buttonNewDelivery = new System.Windows.Forms.Button();
            this.buttonRateDelivery = new System.Windows.Forms.Button();
            this.buttonDelivers = new System.Windows.Forms.Button();
            this.buttonAbout = new System.Windows.Forms.Button();
            this.pictureBoxPhoto = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.buttonRefreshListOfNeeds = new System.Windows.Forms.Button();
            this.groupBoxFunctional.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfNeeds)).BeginInit();
            this.groupBoxNewsletter.SuspendLayout();
            this.panelData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxFunctional
            // 
            this.groupBoxFunctional.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFunctional.AutoSize = true;
            this.groupBoxFunctional.Controls.Add(this.buttonRefreshListOfNeeds);
            this.groupBoxFunctional.Controls.Add(this.dataGridViewListOfNeeds);
            this.groupBoxFunctional.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBoxFunctional.Location = new System.Drawing.Point(193, 134);
            this.groupBoxFunctional.Name = "groupBoxFunctional";
            this.groupBoxFunctional.Size = new System.Drawing.Size(466, 441);
            this.groupBoxFunctional.TabIndex = 0;
            this.groupBoxFunctional.TabStop = false;
            this.groupBoxFunctional.Text = "Lista potrzeb:";
            // 
            // dataGridViewListOfNeeds
            // 
            this.dataGridViewListOfNeeds.AllowUserToAddRows = false;
            this.dataGridViewListOfNeeds.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewListOfNeeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListOfNeeds.Location = new System.Drawing.Point(6, 35);
            this.dataGridViewListOfNeeds.Name = "dataGridViewListOfNeeds";
            this.dataGridViewListOfNeeds.ReadOnly = true;
            this.dataGridViewListOfNeeds.Size = new System.Drawing.Size(454, 381);
            this.dataGridViewListOfNeeds.TabIndex = 0;
            this.dataGridViewListOfNeeds.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewListOfNeeds_CellFormatting);
            // 
            // labelName
            // 
            this.labelName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(750, 24);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(100, 20);
            this.labelName.TabIndex = 2;
            this.labelName.Text = "Jan Kowalski";
            // 
            // buttonLogout
            // 
            this.buttonLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLogout.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonLogout.Location = new System.Drawing.Point(753, 56);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(97, 23);
            this.buttonLogout.TabIndex = 3;
            this.buttonLogout.Text = "Wyloguj";
            this.buttonLogout.UseVisualStyleBackColor = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // groupBoxNewsletter
            // 
            this.groupBoxNewsletter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxNewsletter.AutoSize = true;
            this.groupBoxNewsletter.Controls.Add(this.textBoxNewsletter);
            this.groupBoxNewsletter.Controls.Add(this.pictureBox2);
            this.groupBoxNewsletter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.groupBoxNewsletter.Location = new System.Drawing.Point(659, 134);
            this.groupBoxNewsletter.Name = "groupBoxNewsletter";
            this.groupBoxNewsletter.Size = new System.Drawing.Size(337, 429);
            this.groupBoxNewsletter.TabIndex = 4;
            this.groupBoxNewsletter.TabStop = false;
            this.groupBoxNewsletter.Text = "Newsletter";
            // 
            // textBoxNewsletter
            // 
            this.textBoxNewsletter.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxNewsletter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxNewsletter.Location = new System.Drawing.Point(18, 35);
            this.textBoxNewsletter.Multiline = true;
            this.textBoxNewsletter.Name = "textBoxNewsletter";
            this.textBoxNewsletter.Size = new System.Drawing.Size(298, 71);
            this.textBoxNewsletter.TabIndex = 2;
            // 
            // panelData
            // 
            this.panelData.Controls.Add(this.labelTitle);
            this.panelData.Controls.Add(this.labelName);
            this.panelData.Controls.Add(this.buttonLogout);
            this.panelData.Controls.Add(this.pictureBoxPhoto);
            this.panelData.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelData.Location = new System.Drawing.Point(0, 0);
            this.panelData.Name = "panelData";
            this.panelData.Size = new System.Drawing.Size(1002, 128);
            this.panelData.TabIndex = 5;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.Transparent;
            this.labelTitle.Font = new System.Drawing.Font("Source Code Pro Light", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(12, 5);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(300, 80);
            this.labelTitle.TabIndex = 6;
            this.labelTitle.Text = "HurtSys";
            // 
            // buttonNewDelivery
            // 
            this.buttonNewDelivery.BackColor = System.Drawing.Color.SeaGreen;
            this.buttonNewDelivery.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonNewDelivery.Location = new System.Drawing.Point(13, 169);
            this.buttonNewDelivery.Name = "buttonNewDelivery";
            this.buttonNewDelivery.Size = new System.Drawing.Size(161, 63);
            this.buttonNewDelivery.TabIndex = 6;
            this.buttonNewDelivery.Text = "Nowa Dostawa";
            this.buttonNewDelivery.UseVisualStyleBackColor = false;
            this.buttonNewDelivery.Click += new System.EventHandler(this.buttonNewDelivery_Click);
            // 
            // buttonRateDelivery
            // 
            this.buttonRateDelivery.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.buttonRateDelivery.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRateDelivery.Location = new System.Drawing.Point(13, 243);
            this.buttonRateDelivery.Name = "buttonRateDelivery";
            this.buttonRateDelivery.Size = new System.Drawing.Size(161, 63);
            this.buttonRateDelivery.TabIndex = 7;
            this.buttonRateDelivery.Text = "Ocena Dostawy";
            this.buttonRateDelivery.UseVisualStyleBackColor = false;
            this.buttonRateDelivery.Click += new System.EventHandler(this.buttonRateDelivery_Click);
            // 
            // buttonDelivers
            // 
            this.buttonDelivers.BackColor = System.Drawing.Color.SpringGreen;
            this.buttonDelivers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDelivers.Location = new System.Drawing.Point(13, 317);
            this.buttonDelivers.Name = "buttonDelivers";
            this.buttonDelivers.Size = new System.Drawing.Size(161, 63);
            this.buttonDelivers.TabIndex = 8;
            this.buttonDelivers.Text = "Dostawcy";
            this.buttonDelivers.UseVisualStyleBackColor = false;
            this.buttonDelivers.Click += new System.EventHandler(this.buttonDelivers_Click);
            // 
            // buttonAbout
            // 
            this.buttonAbout.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.buttonAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAbout.Location = new System.Drawing.Point(12, 391);
            this.buttonAbout.Name = "buttonAbout";
            this.buttonAbout.Size = new System.Drawing.Size(161, 63);
            this.buttonAbout.TabIndex = 9;
            this.buttonAbout.Text = "O użytkowniku";
            this.buttonAbout.UseVisualStyleBackColor = false;
            this.buttonAbout.Click += new System.EventHandler(this.buttonAbout_Click);
            // 
            // pictureBoxPhoto
            // 
            this.pictureBoxPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxPhoto.Location = new System.Drawing.Point(890, 5);
            this.pictureBoxPhoto.Name = "pictureBoxPhoto";
            this.pictureBoxPhoto.Size = new System.Drawing.Size(105, 116);
            this.pictureBoxPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPhoto.TabIndex = 1;
            this.pictureBoxPhoto.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::HurtSys.Properties.Resources.moutains;
            this.pictureBox2.Location = new System.Drawing.Point(6, 131);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(325, 273);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // buttonRefreshListOfNeeds
            // 
            this.buttonRefreshListOfNeeds.BackgroundImage = global::HurtSys.Properties.Resources.refresh;
            this.buttonRefreshListOfNeeds.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonRefreshListOfNeeds.Location = new System.Drawing.Point(426, 0);
            this.buttonRefreshListOfNeeds.Name = "buttonRefreshListOfNeeds";
            this.buttonRefreshListOfNeeds.Size = new System.Drawing.Size(34, 35);
            this.buttonRefreshListOfNeeds.TabIndex = 1;
            this.buttonRefreshListOfNeeds.UseVisualStyleBackColor = true;
            this.buttonRefreshListOfNeeds.Click += new System.EventHandler(this.buttonRefreshListOfNeeds_Click);
            // 
            // FormHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 562);
            this.Controls.Add(this.buttonAbout);
            this.Controls.Add(this.buttonDelivers);
            this.Controls.Add(this.buttonRateDelivery);
            this.Controls.Add(this.buttonNewDelivery);
            this.Controls.Add(this.panelData);
            this.Controls.Add(this.groupBoxNewsletter);
            this.Controls.Add(this.groupBoxFunctional);
            this.Name = "FormHome";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Hurt Sys v0.1";
            this.groupBoxFunctional.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListOfNeeds)).EndInit();
            this.groupBoxNewsletter.ResumeLayout(false);
            this.groupBoxNewsletter.PerformLayout();
            this.panelData.ResumeLayout(false);
            this.panelData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxFunctional;
        private System.Windows.Forms.PictureBox pictureBoxPhoto;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.DataGridView dataGridViewListOfNeeds;
        private System.Windows.Forms.GroupBox groupBoxNewsletter;
        private System.Windows.Forms.Panel panelData;
        private System.Windows.Forms.Button buttonNewDelivery;
        private System.Windows.Forms.Button buttonRateDelivery;
        private System.Windows.Forms.Button buttonDelivers;
        private System.Windows.Forms.Button buttonAbout;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox textBoxNewsletter;
        private System.Windows.Forms.Button buttonRefreshListOfNeeds;
    }
}