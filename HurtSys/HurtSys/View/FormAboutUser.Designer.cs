﻿namespace HurtSys
{
    partial class FormAboutUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.panelData = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.pictureBoxPhoto = new System.Windows.Forms.PictureBox();
            this.buttonChangeData = new System.Windows.Forms.Button();
            this.buttonChangePassword = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.groupBoxUserData = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxWorkerPhoto = new System.Windows.Forms.PictureBox();
            this.groupBoxPassword = new System.Windows.Forms.GroupBox();
            this.buttonChangePasswordSave = new System.Windows.Forms.Button();
            this.pictureBoxShowPass3 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxVerifyPass = new System.Windows.Forms.TextBox();
            this.pictureBoxShowPass2 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxNewPass = new System.Windows.Forms.TextBox();
            this.pictureBoxShowPass1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxOldPass = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.panelData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).BeginInit();
            this.groupBoxUserData.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWorkerPhoto)).BeginInit();
            this.groupBoxPassword.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowPass3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowPass2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowPass1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(750, 24);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(100, 20);
            this.labelName.TabIndex = 2;
            this.labelName.Text = "Jan Kowalski";
            // 
            // buttonLogout
            // 
            this.buttonLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLogout.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonLogout.Location = new System.Drawing.Point(753, 56);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(97, 23);
            this.buttonLogout.TabIndex = 3;
            this.buttonLogout.Text = "Wyloguj";
            this.buttonLogout.UseVisualStyleBackColor = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // panelData
            // 
            this.panelData.Controls.Add(this.labelTitle);
            this.panelData.Controls.Add(this.labelName);
            this.panelData.Controls.Add(this.buttonLogout);
            this.panelData.Controls.Add(this.pictureBoxPhoto);
            this.panelData.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelData.Location = new System.Drawing.Point(0, 0);
            this.panelData.Name = "panelData";
            this.panelData.Size = new System.Drawing.Size(1002, 128);
            this.panelData.TabIndex = 5;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.Transparent;
            this.labelTitle.Font = new System.Drawing.Font("Source Code Pro Light", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(12, 5);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(300, 80);
            this.labelTitle.TabIndex = 6;
            this.labelTitle.Text = "HurtSys";
            // 
            // pictureBoxPhoto
            // 
            this.pictureBoxPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxPhoto.Location = new System.Drawing.Point(890, 5);
            this.pictureBoxPhoto.Name = "pictureBoxPhoto";
            this.pictureBoxPhoto.Size = new System.Drawing.Size(105, 116);
            this.pictureBoxPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPhoto.TabIndex = 1;
            this.pictureBoxPhoto.TabStop = false;
            // 
            // buttonChangeData
            // 
            this.buttonChangeData.BackColor = System.Drawing.Color.SeaGreen;
            this.buttonChangeData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonChangeData.Location = new System.Drawing.Point(13, 169);
            this.buttonChangeData.Name = "buttonChangeData";
            this.buttonChangeData.Size = new System.Drawing.Size(161, 63);
            this.buttonChangeData.TabIndex = 6;
            this.buttonChangeData.Text = "Zmień dane";
            this.buttonChangeData.UseVisualStyleBackColor = false;
            this.buttonChangeData.Click += new System.EventHandler(this.buttonChangeData_Click);
            // 
            // buttonChangePassword
            // 
            this.buttonChangePassword.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.buttonChangePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonChangePassword.Location = new System.Drawing.Point(13, 243);
            this.buttonChangePassword.Name = "buttonChangePassword";
            this.buttonChangePassword.Size = new System.Drawing.Size(161, 63);
            this.buttonChangePassword.TabIndex = 7;
            this.buttonChangePassword.Text = "Zmodyfikuj hasło";
            this.buttonChangePassword.UseVisualStyleBackColor = false;
            this.buttonChangePassword.Click += new System.EventHandler(this.buttonChangePassword_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.SpringGreen;
            this.buttonBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBack.Location = new System.Drawing.Point(13, 478);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(161, 63);
            this.buttonBack.TabIndex = 8;
            this.buttonBack.Text = "Powrót";
            this.buttonBack.UseVisualStyleBackColor = false;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // groupBoxUserData
            // 
            this.groupBoxUserData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxUserData.Controls.Add(this.label4);
            this.groupBoxUserData.Controls.Add(this.label3);
            this.groupBoxUserData.Controls.Add(this.label2);
            this.groupBoxUserData.Controls.Add(this.label1);
            this.groupBoxUserData.Controls.Add(this.textBoxPhone);
            this.groupBoxUserData.Controls.Add(this.textBoxEmail);
            this.groupBoxUserData.Controls.Add(this.textBoxSurname);
            this.groupBoxUserData.Controls.Add(this.textBoxName);
            this.groupBoxUserData.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxUserData.Location = new System.Drawing.Point(222, 149);
            this.groupBoxUserData.Name = "groupBoxUserData";
            this.groupBoxUserData.Size = new System.Drawing.Size(461, 413);
            this.groupBoxUserData.TabIndex = 9;
            this.groupBoxUserData.TabStop = false;
            this.groupBoxUserData.Text = "Dane użytkownika";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 323);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 25);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nr kontaktowy";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 243);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "E-mail:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nazwisko:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "Imię:";
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Enabled = false;
            this.textBoxPhone.Location = new System.Drawing.Point(213, 317);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(228, 31);
            this.textBoxPhone.TabIndex = 4;
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Enabled = false;
            this.textBoxEmail.Location = new System.Drawing.Point(213, 237);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(228, 31);
            this.textBoxEmail.TabIndex = 3;
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Enabled = false;
            this.textBoxSurname.Location = new System.Drawing.Point(213, 157);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(228, 31);
            this.textBoxSurname.TabIndex = 2;
            // 
            // textBoxName
            // 
            this.textBoxName.Enabled = false;
            this.textBoxName.Location = new System.Drawing.Point(213, 77);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(228, 31);
            this.textBoxName.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.pictureBoxWorkerPhoto);
            this.panel1.Location = new System.Drawing.Point(690, 149);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(305, 413);
            this.panel1.TabIndex = 10;
            // 
            // pictureBoxWorkerPhoto
            // 
            this.pictureBoxWorkerPhoto.Location = new System.Drawing.Point(18, 48);
            this.pictureBoxWorkerPhoto.Name = "pictureBoxWorkerPhoto";
            this.pictureBoxWorkerPhoto.Size = new System.Drawing.Size(282, 317);
            this.pictureBoxWorkerPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxWorkerPhoto.TabIndex = 2;
            this.pictureBoxWorkerPhoto.TabStop = false;
            // 
            // groupBoxPassword
            // 
            this.groupBoxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxPassword.Controls.Add(this.buttonChangePasswordSave);
            this.groupBoxPassword.Controls.Add(this.pictureBoxShowPass3);
            this.groupBoxPassword.Controls.Add(this.label7);
            this.groupBoxPassword.Controls.Add(this.textBoxVerifyPass);
            this.groupBoxPassword.Controls.Add(this.pictureBoxShowPass2);
            this.groupBoxPassword.Controls.Add(this.label6);
            this.groupBoxPassword.Controls.Add(this.textBoxNewPass);
            this.groupBoxPassword.Controls.Add(this.pictureBoxShowPass1);
            this.groupBoxPassword.Controls.Add(this.label5);
            this.groupBoxPassword.Controls.Add(this.textBoxOldPass);
            this.groupBoxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.groupBoxPassword.Location = new System.Drawing.Point(222, 131);
            this.groupBoxPassword.Name = "groupBoxPassword";
            this.groupBoxPassword.Size = new System.Drawing.Size(461, 424);
            this.groupBoxPassword.TabIndex = 11;
            this.groupBoxPassword.TabStop = false;
            this.groupBoxPassword.Text = "Hasło";
            this.groupBoxPassword.Visible = false;
            // 
            // buttonChangePasswordSave
            // 
            this.buttonChangePasswordSave.Location = new System.Drawing.Point(114, 365);
            this.buttonChangePasswordSave.Name = "buttonChangePasswordSave";
            this.buttonChangePasswordSave.Size = new System.Drawing.Size(285, 33);
            this.buttonChangePasswordSave.TabIndex = 20;
            this.buttonChangePasswordSave.Text = "Zatwierdź zmianę hasła";
            this.buttonChangePasswordSave.UseVisualStyleBackColor = true;
            this.buttonChangePasswordSave.Click += new System.EventHandler(this.buttonChangePasswordSave_Click);
            // 
            // pictureBoxShowPass3
            // 
            this.pictureBoxShowPass3.Image = global::HurtSys.Properties.Resources.show_pass;
            this.pictureBoxShowPass3.Location = new System.Drawing.Point(406, 274);
            this.pictureBoxShowPass3.Name = "pictureBoxShowPass3";
            this.pictureBoxShowPass3.Size = new System.Drawing.Size(28, 31);
            this.pictureBoxShowPass3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxShowPass3.TabIndex = 19;
            this.pictureBoxShowPass3.TabStop = false;
            this.pictureBoxShowPass3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.showValidationPass);
            this.pictureBoxShowPass3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.hideValidationPass);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 277);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 25);
            this.label7.TabIndex = 18;
            this.label7.Text = "Powtórz hasło";
            // 
            // textBoxVerifyPass
            // 
            this.textBoxVerifyPass.Location = new System.Drawing.Point(189, 274);
            this.textBoxVerifyPass.Name = "textBoxVerifyPass";
            this.textBoxVerifyPass.PasswordChar = '*';
            this.textBoxVerifyPass.Size = new System.Drawing.Size(210, 31);
            this.textBoxVerifyPass.TabIndex = 17;
            // 
            // pictureBoxShowPass2
            // 
            this.pictureBoxShowPass2.Image = global::HurtSys.Properties.Resources.show_pass;
            this.pictureBoxShowPass2.Location = new System.Drawing.Point(406, 176);
            this.pictureBoxShowPass2.Name = "pictureBoxShowPass2";
            this.pictureBoxShowPass2.Size = new System.Drawing.Size(28, 31);
            this.pictureBoxShowPass2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxShowPass2.TabIndex = 16;
            this.pictureBoxShowPass2.TabStop = false;
            this.pictureBoxShowPass2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.showNewPass);
            this.pictureBoxShowPass2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.hideNewPass);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 25);
            this.label6.TabIndex = 15;
            this.label6.Text = "Nowe hasło:";
            // 
            // textBoxNewPass
            // 
            this.textBoxNewPass.Location = new System.Drawing.Point(189, 176);
            this.textBoxNewPass.Name = "textBoxNewPass";
            this.textBoxNewPass.PasswordChar = '*';
            this.textBoxNewPass.Size = new System.Drawing.Size(210, 31);
            this.textBoxNewPass.TabIndex = 14;
            // 
            // pictureBoxShowPass1
            // 
            this.pictureBoxShowPass1.Image = global::HurtSys.Properties.Resources.show_pass;
            this.pictureBoxShowPass1.Location = new System.Drawing.Point(406, 78);
            this.pictureBoxShowPass1.Name = "pictureBoxShowPass1";
            this.pictureBoxShowPass1.Size = new System.Drawing.Size(28, 31);
            this.pictureBoxShowPass1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxShowPass1.TabIndex = 13;
            this.pictureBoxShowPass1.TabStop = false;
            this.pictureBoxShowPass1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.showOldPass);
            this.pictureBoxShowPass1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.hideOldPass);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 25);
            this.label5.TabIndex = 12;
            this.label5.Text = "Stare hasło:";
            // 
            // textBoxOldPass
            // 
            this.textBoxOldPass.Location = new System.Drawing.Point(189, 78);
            this.textBoxOldPass.Name = "textBoxOldPass";
            this.textBoxOldPass.PasswordChar = '*';
            this.textBoxOldPass.Size = new System.Drawing.Size(210, 31);
            this.textBoxOldPass.TabIndex = 11;
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSave.Location = new System.Drawing.Point(13, 399);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(161, 63);
            this.buttonSave.TabIndex = 12;
            this.buttonSave.Text = "Zapisz";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // FormAboutUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 562);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.groupBoxPassword);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBoxUserData);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonChangePassword);
            this.Controls.Add(this.buttonChangeData);
            this.Controls.Add(this.panelData);
            this.Name = "FormAboutUser";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Hurt Sys v0.1";
            this.panelData.ResumeLayout(false);
            this.panelData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).EndInit();
            this.groupBoxUserData.ResumeLayout(false);
            this.groupBoxUserData.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWorkerPhoto)).EndInit();
            this.groupBoxPassword.ResumeLayout(false);
            this.groupBoxPassword.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowPass3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowPass2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowPass1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxPhoto;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Panel panelData;
        private System.Windows.Forms.Button buttonChangeData;
        private System.Windows.Forms.Button buttonChangePassword;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.GroupBox groupBoxUserData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxWorkerPhoto;
        private System.Windows.Forms.GroupBox groupBoxPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxOldPass;
        private System.Windows.Forms.PictureBox pictureBoxShowPass3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxVerifyPass;
        private System.Windows.Forms.PictureBox pictureBoxShowPass2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxNewPass;
        private System.Windows.Forms.PictureBox pictureBoxShowPass1;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonChangePasswordSave;
    }
}