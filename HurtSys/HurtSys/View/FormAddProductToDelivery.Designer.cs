﻿namespace HurtSys.View
{
    partial class FormAddProductToDelivery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewItemsOfNeeds = new System.Windows.Forms.DataGridView();
            this.textBoxProduct = new System.Windows.Forms.TextBox();
            this.buttonSearchForItem = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewDeliversRateSystem = new System.Windows.Forms.DataGridView();
            this.dataGridViewPriceSystem = new System.Windows.Forms.DataGridView();
            this.numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItemsOfNeeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDeliversRateSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPriceSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewItemsOfNeeds
            // 
            this.dataGridViewItemsOfNeeds.AllowUserToAddRows = false;
            this.dataGridViewItemsOfNeeds.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewItemsOfNeeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewItemsOfNeeds.Location = new System.Drawing.Point(125, 32);
            this.dataGridViewItemsOfNeeds.Name = "dataGridViewItemsOfNeeds";
            this.dataGridViewItemsOfNeeds.ReadOnly = true;
            this.dataGridViewItemsOfNeeds.Size = new System.Drawing.Size(668, 156);
            this.dataGridViewItemsOfNeeds.TabIndex = 0;
            this.dataGridViewItemsOfNeeds.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewItemsOfNeeds_CellMouseDoubleClick);
            // 
            // textBoxProduct
            // 
            this.textBoxProduct.Location = new System.Drawing.Point(125, 210);
            this.textBoxProduct.Multiline = true;
            this.textBoxProduct.Name = "textBoxProduct";
            this.textBoxProduct.Size = new System.Drawing.Size(492, 26);
            this.textBoxProduct.TabIndex = 1;
            // 
            // buttonSearchForItem
            // 
            this.buttonSearchForItem.BackgroundImage = global::HurtSys.Properties.Resources.loop;
            this.buttonSearchForItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonSearchForItem.Location = new System.Drawing.Point(743, 209);
            this.buttonSearchForItem.Name = "buttonSearchForItem";
            this.buttonSearchForItem.Size = new System.Drawing.Size(50, 27);
            this.buttonSearchForItem.TabIndex = 3;
            this.buttonSearchForItem.UseVisualStyleBackColor = true;
            this.buttonSearchForItem.Click += new System.EventHandler(this.buttonSearchForItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(121, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "System oceny dostawcy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(651, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 24);
            this.label2.TabIndex = 5;
            this.label2.Text = "System cenowy";
            // 
            // dataGridViewDeliversRateSystem
            // 
            this.dataGridViewDeliversRateSystem.AllowUserToAddRows = false;
            this.dataGridViewDeliversRateSystem.AllowUserToDeleteRows = false;
            this.dataGridViewDeliversRateSystem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridViewDeliversRateSystem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDeliversRateSystem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDeliversRateSystem.Location = new System.Drawing.Point(23, 291);
            this.dataGridViewDeliversRateSystem.Name = "dataGridViewDeliversRateSystem";
            this.dataGridViewDeliversRateSystem.ReadOnly = true;
            this.dataGridViewDeliversRateSystem.Size = new System.Drawing.Size(441, 402);
            this.dataGridViewDeliversRateSystem.TabIndex = 6;
            this.dataGridViewDeliversRateSystem.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewDeliversRateSystem_CellMouseDoubleClick);
            // 
            // dataGridViewPriceSystem
            // 
            this.dataGridViewPriceSystem.AllowUserToAddRows = false;
            this.dataGridViewPriceSystem.AllowUserToDeleteRows = false;
            this.dataGridViewPriceSystem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewPriceSystem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPriceSystem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPriceSystem.Location = new System.Drawing.Point(494, 291);
            this.dataGridViewPriceSystem.Name = "dataGridViewPriceSystem";
            this.dataGridViewPriceSystem.ReadOnly = true;
            this.dataGridViewPriceSystem.Size = new System.Drawing.Size(441, 402);
            this.dataGridViewPriceSystem.TabIndex = 7;
            this.dataGridViewPriceSystem.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewPriceSystem_CellMouseDoubleClick);
            // 
            // numericUpDown
            // 
            this.numericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numericUpDown.Location = new System.Drawing.Point(655, 209);
            this.numericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown.Name = "numericUpDown";
            this.numericUpDown.Size = new System.Drawing.Size(69, 29);
            this.numericUpDown.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(424, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 24);
            this.label3.TabIndex = 9;
            this.label3.Text = "Lista potrzeb:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(122, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Podaj nazwę produktu:";
            // 
            // FormAddProductToDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 705);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDown);
            this.Controls.Add(this.dataGridViewPriceSystem);
            this.Controls.Add(this.dataGridViewDeliversRateSystem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSearchForItem);
            this.Controls.Add(this.textBoxProduct);
            this.Controls.Add(this.dataGridViewItemsOfNeeds);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormAddProductToDelivery";
            this.Text = "Hurt Sys v0.1 - Dodawanie produktu do zamówienia";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItemsOfNeeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDeliversRateSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPriceSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewItemsOfNeeds;
        private System.Windows.Forms.TextBox textBoxProduct;
        private System.Windows.Forms.Button buttonSearchForItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewDeliversRateSystem;
        private System.Windows.Forms.DataGridView dataGridViewPriceSystem;
        private System.Windows.Forms.NumericUpDown numericUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}