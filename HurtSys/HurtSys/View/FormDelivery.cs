﻿using System;
using System.Windows.Forms;
using HurtSys.Controler;
using HurtSys.Model;
using HurtSys.Resources;
using HurtSys.View;

namespace HurtSys
{
    /// <summary>
    /// Formularz tworzenia dostawy
    /// </summary>
    public partial class FormDelivery : Form
    {
        /// <summary>
        /// Konstruktor inicjujący formularz dodawania dostawy
        /// Ustawianie danych użytkownika oraz inicjowanie listy dostawy
        /// </summary>
        public FormDelivery()
        {
            InitializeComponent();
            AccountManager.SetupUserData(labelName, pictureBoxPhoto);
            DeliveryController.SetListDeliveryItemToDataGrid(dataGridViewItems);
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            DeliveryController.CheckIfUserWantToExitApp();
        }

        private void buttonAddProduct_Click(object sender, EventArgs e)
        {
            // usuwanie produktów z listy potrzeb które już są w zamówieniu
            DeliveryController.CleanUpListOfNeeds();

            FormAddProductToDelivery formAddProduct = new FormAddProductToDelivery(this);
            formAddProduct.ShowDialog();
            DeliveryListRefresh();
        }

        private void buttonSummary_Click(object sender, EventArgs e)
        {
            if (DeliveryController.DeliveryItems.Count > 0)
            {
                DeliveryController.CleanUpListOfNeeds();
                DeliveryController.GenerateSummary(textBoxSummary);
                groupBoxSummary.Visible = true;
                groupBoxItems.Visible = false;
            }
            else
            {
                MessageBox.Show(MessageDictionary.NoItemsInDelivery,MessageDictionary.SummaryUnreachable);
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            if (groupBoxItems.Visible)
            {
                if(DeliveryController.CheckIfUserWantToQuitEdition())
                this.Close();
            }
            else
            {
                groupBoxSummary.Visible = false;
                groupBoxItems.Visible = true;
            }
        }

        /// <summary>
        /// Odświeżanie listy dostawy
        /// </summary>
        public  void DeliveryListRefresh()
        {
            dataGridViewItems.Update();
            dataGridViewItems.Refresh();
        }

        private void dataGridViewItems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DeliveryController.DeleteDeliveryPosition(e);
        }

        private void buttonSendDelivery_Click(object sender, EventArgs e)
        {
            buttonAddProduct.Enabled = false;
            FormSendDelivery formSendDelivery = new FormSendDelivery(this);
            formSendDelivery.ShowDialog();
        }
    }
}
