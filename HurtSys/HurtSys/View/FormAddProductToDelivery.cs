﻿using System;
using System.Windows.Forms;
using HurtSys.Controler;
using HurtSys.Resources;

namespace HurtSys.View
{
    /// <summary>
    /// Formluarz pozwalający na dodanie produktu do dostawy
    /// </summary>
    public partial class FormAddProductToDelivery : Form
    {
        private readonly FormDelivery formDelivery;

        /// <summary>
        /// Konstruktor inicjujący formularz dodawania produtku do dostawy
        /// </summary>
        /// <param name="form">Formularz wywołujący dodawanie produktu</param>
        public FormAddProductToDelivery(FormDelivery form)
        {
            InitializeComponent();
            formDelivery = form;
            AddProductToDeliveryController.SetItemsOfNeedsToDataGridView(dataGridViewItemsOfNeeds);
        }

        private void buttonSearchForItem_Click(object sender, EventArgs e)
        {
            if (numericUpDown.Value > numericUpDown.Minimum)
            {
                AddProductToDeliveryController.SearchItemInPriceSystem(dataGridViewPriceSystem, textBoxProduct.Text);
                AddProductToDeliveryController.SearchItemInRateSystem(dataGridViewDeliversRateSystem, textBoxProduct.Text);

                if (AddProductToDeliveryController.NoSearchResults())
                {
                    MessageBox.Show(MessageDictionary.CannotFindItemByName + textBoxProduct.Text, MessageDictionary.NoItemByName);
                }
            }
            else
            {
                MessageBox.Show(MessageDictionary.ZeroAmount);
            }
        }


        private void dataGridViewPriceSystem_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                DeliveryController.DeliveryItems.Add(AddProductToDeliveryController.dataGridViewPriceSystem_AddProductToDelivery(dataGridViewPriceSystem, e, numericUpDown));
                this.Close();
                formDelivery.DeliveryListRefresh();
            }
        }

        private void dataGridViewDeliversRateSystem_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                DeliveryController.DeliveryItems.Add(AddProductToDeliveryController.dataGridViewDeliversRateSystem_AddProductToDelivery(dataGridViewDeliversRateSystem, e, numericUpDown));
                this.Close();
                formDelivery.DeliveryListRefresh();
            }
        }

        private void dataGridViewItemsOfNeeds_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex > -1)
            {
                textBoxProduct.Text = dataGridViewItemsOfNeeds.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            }
        }
    }
}
