﻿namespace HurtSys
{
    partial class FormDelivers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxPhoto = new System.Windows.Forms.PictureBox();
            this.labelName = new System.Windows.Forms.Label();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.panelData = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.buttonNewDeliver = new System.Windows.Forms.Button();
            this.buttonDeleteDeliver = new System.Windows.Forms.Button();
            this.buttonEditDeliver = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.dataGridViewDelivers = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).BeginInit();
            this.panelData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDelivers)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxPhoto
            // 
            this.pictureBoxPhoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxPhoto.Location = new System.Drawing.Point(890, 5);
            this.pictureBoxPhoto.Name = "pictureBoxPhoto";
            this.pictureBoxPhoto.Size = new System.Drawing.Size(105, 116);
            this.pictureBoxPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxPhoto.TabIndex = 1;
            this.pictureBoxPhoto.TabStop = false;
            // 
            // labelName
            // 
            this.labelName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.Location = new System.Drawing.Point(750, 24);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(100, 20);
            this.labelName.TabIndex = 2;
            this.labelName.Text = "Jan Kowalski";
            // 
            // buttonLogout
            // 
            this.buttonLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLogout.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonLogout.Location = new System.Drawing.Point(753, 56);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(97, 23);
            this.buttonLogout.TabIndex = 3;
            this.buttonLogout.Text = "Wyloguj";
            this.buttonLogout.UseVisualStyleBackColor = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // panelData
            // 
            this.panelData.Controls.Add(this.labelTitle);
            this.panelData.Controls.Add(this.labelName);
            this.panelData.Controls.Add(this.buttonLogout);
            this.panelData.Controls.Add(this.pictureBoxPhoto);
            this.panelData.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelData.Location = new System.Drawing.Point(0, 0);
            this.panelData.Name = "panelData";
            this.panelData.Size = new System.Drawing.Size(1002, 128);
            this.panelData.TabIndex = 5;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.Transparent;
            this.labelTitle.Font = new System.Drawing.Font("Source Code Pro Light", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTitle.Location = new System.Drawing.Point(12, 5);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(300, 80);
            this.labelTitle.TabIndex = 6;
            this.labelTitle.Text = "HurtSys";
            // 
            // buttonNewDeliver
            // 
            this.buttonNewDeliver.BackColor = System.Drawing.Color.SeaGreen;
            this.buttonNewDeliver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonNewDeliver.Location = new System.Drawing.Point(13, 169);
            this.buttonNewDeliver.Name = "buttonNewDeliver";
            this.buttonNewDeliver.Size = new System.Drawing.Size(161, 63);
            this.buttonNewDeliver.TabIndex = 6;
            this.buttonNewDeliver.Text = "Nowy Dostawca";
            this.buttonNewDeliver.UseVisualStyleBackColor = false;
            this.buttonNewDeliver.Click += new System.EventHandler(this.buttonNewDeliver_Click);
            // 
            // buttonDeleteDeliver
            // 
            this.buttonDeleteDeliver.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.buttonDeleteDeliver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDeleteDeliver.Location = new System.Drawing.Point(13, 243);
            this.buttonDeleteDeliver.Name = "buttonDeleteDeliver";
            this.buttonDeleteDeliver.Size = new System.Drawing.Size(161, 63);
            this.buttonDeleteDeliver.TabIndex = 7;
            this.buttonDeleteDeliver.Text = "Usuń dostawcę";
            this.buttonDeleteDeliver.UseVisualStyleBackColor = false;
            this.buttonDeleteDeliver.Click += new System.EventHandler(this.buttonDeleteDeliver_Click);
            // 
            // buttonEditDeliver
            // 
            this.buttonEditDeliver.BackColor = System.Drawing.Color.SpringGreen;
            this.buttonEditDeliver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEditDeliver.Location = new System.Drawing.Point(13, 317);
            this.buttonEditDeliver.Name = "buttonEditDeliver";
            this.buttonEditDeliver.Size = new System.Drawing.Size(161, 63);
            this.buttonEditDeliver.TabIndex = 8;
            this.buttonEditDeliver.Text = "Edytuj dostawcę";
            this.buttonEditDeliver.UseVisualStyleBackColor = false;
            this.buttonEditDeliver.Click += new System.EventHandler(this.buttonEditDeliver_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonClose.Location = new System.Drawing.Point(12, 453);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(161, 63);
            this.buttonClose.TabIndex = 9;
            this.buttonClose.Text = "Zamknij";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // dataGridViewDelivers
            // 
            this.dataGridViewDelivers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewDelivers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDelivers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDelivers.Location = new System.Drawing.Point(208, 192);
            this.dataGridViewDelivers.Name = "dataGridViewDelivers";
            this.dataGridViewDelivers.Size = new System.Drawing.Size(760, 333);
            this.dataGridViewDelivers.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label1.Location = new System.Drawing.Point(204, 154);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 24);
            this.label1.TabIndex = 11;
            this.label1.Text = "Nasi Dostawcy:";
            // 
            // FormDelivers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1002, 562);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewDelivers);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonEditDeliver);
            this.Controls.Add(this.buttonDeleteDeliver);
            this.Controls.Add(this.buttonNewDeliver);
            this.Controls.Add(this.panelData);
            this.Name = "FormDelivers";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Hurt Sys v0.1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).EndInit();
            this.panelData.ResumeLayout(false);
            this.panelData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDelivers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBoxPhoto;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Panel panelData;
        private System.Windows.Forms.Button buttonNewDeliver;
        private System.Windows.Forms.Button buttonDeleteDeliver;
        private System.Windows.Forms.Button buttonEditDeliver;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.DataGridView dataGridViewDelivers;
        private System.Windows.Forms.Label label1;
    }
}