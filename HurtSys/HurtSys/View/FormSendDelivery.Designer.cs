﻿namespace HurtSys.View
{
    partial class FormSendDelivery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.progressBarSendDelivery = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBoxExtras = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxExtras)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBarSendDelivery
            // 
            this.progressBarSendDelivery.Location = new System.Drawing.Point(0, 356);
            this.progressBarSendDelivery.Name = "progressBarSendDelivery";
            this.progressBarSendDelivery.Size = new System.Drawing.Size(647, 52);
            this.progressBarSendDelivery.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(376, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Przesyłanie informacji do dostawców..";
            // 
            // pictureBoxExtras
            // 
            this.pictureBoxExtras.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxExtras.Image = global::HurtSys.Properties.Resources.giphy;
            this.pictureBoxExtras.Location = new System.Drawing.Point(158, 96);
            this.pictureBoxExtras.Name = "pictureBoxExtras";
            this.pictureBoxExtras.Size = new System.Drawing.Size(315, 194);
            this.pictureBoxExtras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxExtras.TabIndex = 1;
            this.pictureBoxExtras.TabStop = false;
            this.pictureBoxExtras.Click += new System.EventHandler(this.pictureBoxExtras_Click);
            // 
            // timer
            // 
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick_1);
            // 
            // FormSendDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(647, 410);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBoxExtras);
            this.Controls.Add(this.progressBarSendDelivery);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormSendDelivery";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSendDelivery_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxExtras)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBarSendDelivery;
        private System.Windows.Forms.PictureBox pictureBoxExtras;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
    }
}