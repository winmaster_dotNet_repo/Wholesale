﻿using System;
using System.Windows.Forms;
using HurtSys.Model;
using HurtSys.Resources;

namespace HurtSys
{
    /// <summary>
    /// Formularz logowania do Systemu HurtSys
    /// </summary>
    public partial class FormLogin : Form
    {
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (AccountManager.LoginQuerry(textBoxLogin.Text, textBoxPassword.Text))
                {
                    MessageBox.Show(MessageDictionary.Welcome + AccountManager.hurtSysUser.UsrName);
                    FormHome formHome = new FormHome();
                    formHome.Show();
                }
                else
                {
                    MessageBox.Show(MessageDictionary.LoginFailed+"\n"+MessageDictionary.IncorrectLogin);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(MessageDictionary.LoginFailed + "\n"  + ex.Message + "\n\n"+MessageDictionary.PWRAlert);
            }
        }

        /// <summary>
        /// Konstruktor formularza logowania
        /// </summary>
        public FormLogin()
        {
            InitializeComponent();
        }
    }
}