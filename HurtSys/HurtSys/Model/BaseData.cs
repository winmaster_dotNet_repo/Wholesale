﻿using System.Collections.Generic;
using System.Configuration;

namespace HurtSys.Model
{
    /// <summary>
    /// Klasa modelu przechowująca podstawowe dane wspólne dla programu
    /// </summary>
    public class BaseData
    {
        /// <summary>
        /// ConnectionString do połączenia z bazą danych Azure
        /// </summary>
        public static string connectionString = ConfigurationManager.ConnectionStrings["HurtSysConnect"].ConnectionString;
        /// <summary>
        /// Dynamiczna Lista potrzeb produktowych
        /// </summary>
        public static List<dynamic> ListOfItemsOfNeeds = new List<dynamic>();


        /// <summary>
        /// Lokalizacja pliku resx zawierającego teksty newslettera
        /// </summary>
        public const string resxTexts = @".\Resources\Texts.resx";
        /// <summary>
        /// Lokalizacja pliku resx zawierającego zdjęcia pracowników
        /// </summary>
        public const string resxWorkerPhoto = @".\Resources\Photos.resx";
    }
}
