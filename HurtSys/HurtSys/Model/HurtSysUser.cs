﻿namespace HurtSys.Model
{
    /// <summary>
    /// Model reprezentujący użytkownika systemu HurtSys
    /// </summary>
    public class HurtSysUser
    {
        /// <summary>
        /// Login użytkownika systemu HurtSys
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Hasło użytkownika systemu HurtSys
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Imię użytkownika systemu HurtSys
        /// </summary>
        public string UsrName { get; set; }
        /// <summary>
        /// Nazwisko użytkownika systemu HurtSys
        /// </summary>
        public string UsrSurname { get; set; }
        /// <summary>
        /// Email użytkownika systemu HurtSys
        /// </summary>
        public string UsrEmail { get; set; }
        /// <summary>
        /// Telefon użytkownika systemu HurtSys
        /// </summary>
        public string UsrPhone { get; set; }
        /// <summary>
        /// ID w systemie użytkownika systemu HurtSys
        /// </summary>
        public int ID { get; set; }


        //implementacja Singletona HurtSysUser

        private static HurtSysUser instance;

        private HurtSysUser() { }

        /// <summary>
        /// Konstruktor HurtSysUsera, klasy singleton
        /// </summary>
        public static HurtSysUser Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new HurtSysUser();
                }
                return instance;
            }
        }
    }
}