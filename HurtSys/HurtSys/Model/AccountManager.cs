﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;
using HurtSys.Controler;
using HurtSys.Resources;

namespace HurtSys.Model
{
    /// <summary>
    /// Klasa modelu
    /// </summary>
    public class AccountManager
    {
        /// <summary>
        /// Użytkownik systemu
        /// </summary>
        public static HurtSysUser hurtSysUser;
        /// <summary>
        /// Numer zdjęcia użytkownika
        /// </summary>
        public static int userPhotoNumber;

        /// <summary>
        /// Metoda obsługująca logowanie do systemu HurtSys
        /// </summary>
        /// <param name="login_">Login użytkownika</param>
        /// <param name="password_">Hasło użytkownika</param>
        /// <returns>Zwraca true jeśki logowanie się powiodło, przeciwnie zwraca false</returns>
        public static bool LoginQuerry(string login_, string password_)
        {
            string passwordSha = BaseController.sha256_hash(password_);

            using (var connection = new SqlConnection(BaseData.connectionString))
            {
                var @params = new
                {
                    login = login_,
                    password = passwordSha,
                };
                hurtSysUser = connection.Query<HurtSysUser>(SQLDictionary.LoginQuerry, @params).SingleOrDefault();
            }

            if (hurtSysUser != null)
            {
                LoadWorkerPhotos();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Wybiera nr zdjęcia dla użytkownika
        /// </summary>
        public static void SetUserPhotoNumber()
        {
            Random rnd = new Random();
            userPhotoNumber = rnd.Next(0, BaseController.ListOfWorkerPhotosResource.Count);
        }

        /// <summary>
        /// Funkcja ładuje zdjęcia użytkowników z resources
        /// </summary>
        public static void LoadWorkerPhotos()
        {
            using (ResXResourceSet resxSet = new ResXResourceSet(BaseData.resxWorkerPhoto))
            {
                foreach (DictionaryEntry entry in resxSet)
                {
                    BaseController.ListOfWorkerPhotosResource.Add((string) entry.Key);
                }
            }
            SetUserPhotoNumber();
        }

        /// <summary>
        /// Ustawianie zdjęcia dla użytkownika
        /// </summary>
        /// <param name="photo">Kontener na fotografię</param>
        public static void SetUserPhoto(PictureBox photo)
        {
            using (ResXResourceSet resxSet = new ResXResourceSet(BaseData.resxWorkerPhoto))
            {
                photo.Image = (Image)resxSet.GetObject(BaseController.ListOfWorkerPhotosResource[userPhotoNumber], true);
            }
        }

        /// <summary>
        /// Metoda ustawiająca dane użytkownika po zalogowaniu
        /// </summary>
        /// <param name="name">Kontener na imię usera</param>
        /// <param name="photo">Kontener na fotografię usera</param>
        public static void SetupUserData(System.Windows.Forms.Label name, PictureBox photo)
        {
            name.Text = AccountManager.hurtSysUser.UsrName + " " + AccountManager.hurtSysUser.UsrSurname;

            SetUserPhoto(photo);
        }
    }
}