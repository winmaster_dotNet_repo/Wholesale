﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using HurtSys.Annotations;

namespace HurtSys.Model
{
    /// <summary>
    /// Model reprezentujący Przedmiot Dostawy
    /// </summary>
    public class DeliveryItem : INotifyPropertyChanged
    {
        private decimal _priceForAmount;

        /// <summary>
        /// Nazwa przedmiotu
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// Dostawca przedmiotu
        /// </summary>
        public string ItemDeliver { get; set; }
        /// <summary>
        /// Ilość przedmiotu
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// Cena przedmiotu jednostkowa
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Cena przedmitu w odniesieniu do ilości
        /// </summary>
        public decimal PriceForAmount
        {
            get => _priceForAmount;
            set => _priceForAmount = Amount * Price;
        }

        /// <summary>
        /// Implementacja interfejsu wspomagającego automatyczne nadpisywanie wartości zmiennych
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
