﻿
namespace HurtSys.Model
{
    /// <summary>
    /// Klasa modelu reprezentująca Dostawców
    /// </summary>
    public class Delivers
    {
        /// <summary>
        /// Nazwa dostawcy
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Adres dostawcy
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Dane kontaktowe dostawcy
        /// </summary>
        public string AddressContact { get; set; }
    }
}