﻿
namespace HurtSys.Model
{
    /// <summary>
    /// Model reprezentujący Artykuł
    /// </summary>
    public class Item
    {
        /// <summary>
        ///  ID Artykułu w systemie
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Użytkownik który wprowadził artykuł
        /// </summary>
        public int UserID { get; set; }
        /// <summary>
        /// Nazwa Artykułu
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Minimalna ilość artykułu
        /// </summary>
        public int MinAmount { get; set; }
        /// <summary>
        /// Maksymalna ilość artykułu
        /// </summary>
        public int MaxAmount { get; set; }
        /// <summary>
        /// Jednostka artykułu
        /// </summary>
        public int Unit { get; set; }
        /// <summary>
        ///  Czy dany artykuł podlega sprzedaży
        /// </summary>
        public bool IfWeSell { get; set; }
    }
}
