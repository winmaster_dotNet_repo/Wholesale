﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using Dapper;
using HurtSys.Controler;
using HurtSys.Resources;

namespace HurtSys.Model
{
    /// <summary>
    /// Klasa modelu łącząca się z bazą danych i wykonująca zapytania
    /// </summary>
    public class DbQueries
    {
        #region UserAccount

        /// <summary>
        /// Zapytanie aktualizujące dane użytkownika
        /// </summary>
        /// <param name="name">Imię użytkownika</param>
        /// <param name="surname">Nazwisko użytkownika</param>
        /// <param name="email">Email użytkownika</param>
        /// <param name="phone">Telefon użytkownika</param>
        public static void UpdateUserPersonalData(TextBox name, TextBox surname, TextBox email, TextBox phone)
        {
            using (var connection = new SqlConnection(BaseData.connectionString))
            {
                var @params = new
                {
                    UsrName = name.Text,
                    UsrSurname = surname.Text,
                    UsrEmail = email.Text,
                    UsrPhone = phone.Text,
                    ID = AccountManager.hurtSysUser.ID,
                };
                connection.Execute(SQLDictionary.UpdateUserData, @params);
            }
        }

        /// <summary>
        /// Zapytanie aktualizujące hasło użytkownika
        /// </summary>
        /// <param name="newPass">Nowe hasło użytkownika</param>
        public static void UpdateUserPassword(TextBox newPass)
        {
            string newPassSha = BaseController.sha256_hash(newPass.Text);

            using (var connection = new SqlConnection(BaseData.connectionString))
            {
                var @params = new
                {
                    UsrPass = newPassSha,
                    ID = AccountManager.hurtSysUser.ID,
                };
                connection.Execute(SQLDictionary.UpdateUserPass, @params);
            }
        }

        #endregion

        #region SearchingItems in DB

        /// <summary>
        /// Zapytanie wyszukujące produkt w odniesieniu do systemu cenowego
        /// </summary>
        /// <param name="product">Nazwa produktu</param>
        /// <returns>Zwraca listę dynamiczną produktów</returns>
        public static List<dynamic> SearchItemInPriceSystemQuery(string product)
        {
            using (var connection = new SqlConnection(BaseData.connectionString))
            {
                var @params = new
                {
                    ProdName = product
                };
                return connection.Query(SQLDictionary.SelectProductPriceSystem, @params).ToList();
            }
        }

        /// <summary>
        /// Zapytanie wyszukujące produkt w odniesieniu do systemu oceny dostawcy
        /// </summary>
        /// <param name="product">Nazwa produktu</param>
        /// <returns>Zwraca listę dynamiczną produktów</returns>
        public static List<dynamic> SearchItemInRateSystemQuery(string product)
        {
            using (var connection = new SqlConnection(BaseData.connectionString))
            {
                var @params = new
                {
                    ProdName = product
                };
                return  connection.Query(SQLDictionary.SelectProductRateSystem, @params).ToList();
            }
        }

        /// <summary>
        /// Zapytanie wyszukujące cenę u dostawcy dla konkretnego produktu
        /// </summary>
        /// <param name="item">Nazwa produktu</param>
        /// <param name="deliver">Dostawca</param>
        /// <returns>Zwraca cenę w formacie decimal</returns>
        public static decimal FindPriceForProduct(string item, string deliver)
        {
            using (var connection = new SqlConnection(BaseData.connectionString))
            {
                var @params = new
                {
                    ProdName = item,
                    DeliverName = deliver
                };
                return  connection.Query<decimal>(SQLDictionary.GetPriceOfProduct, @params).SingleOrDefault();
            }
        }

        #endregion

        #region OtherQueries

        /// <summary>
        /// Zapytanie zwracające wszystkich dostawców
        /// </summary>
        /// <returns>Zwraca listę dostawców</returns>
        public static List<Delivers> GetAllDeliversQuery()
        {
            using (var connection = new SqlConnection(BaseData.connectionString))
            {
                return connection.Query<Delivers>(SQLDictionary.GetAllDelivers).ToList();
            }
        }

        /// <summary>
        /// Zapytanie aktualizujące listę potrzeb
        /// </summary>
        public static void ListOfNeedsQuery()
        {
            using (var connection = new SqlConnection(BaseData.connectionString))
            {
                BaseData.ListOfItemsOfNeeds = connection.Query(SQLDictionary.ListOfNeedsItems).ToList<object>();
            }
        }

        #endregion
    }
}
