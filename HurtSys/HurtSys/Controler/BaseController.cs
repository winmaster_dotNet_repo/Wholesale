﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using HurtSys.Resources;

namespace HurtSys.Controler
{
    /// <summary>
    /// Kontener bazowy sterujący
    /// </summary>
    public class BaseController
    {
        /// <summary>
        /// Lista nazw resx zdjęć pracowników
        /// </summary>
        public static List<string> ListOfWorkerPhotosResource = new List<string>();

        /// <summary>
        /// Funkcja konwertująca hasło ze string na string z sha256
        /// </summary>
        /// <param name="value">wartość string hasła</param>
        /// <returns>Zwraca string z hashem hasła w  sha256</returns>
        public static String sha256_hash(String value)
        {
            using (SHA256 hash = SHA256.Create())
            {
                return String.Concat(hash
                    .ComputeHash(Encoding.UTF8.GetBytes(value))
                    .Select(item => item.ToString("x2")));
            }
        }
        
        /// <summary>
        /// Konwerter pomocniczy z kolekcji dynamicznej na  DataTable
        /// </summary>
        /// <param name="items">kolekcja dynamiczna</param>
        /// <returns>kolekcja w postaci DataTable</returns>
        public static DataTable DynamicToDataTable(IEnumerable<dynamic> items)
        {
            var data = items.ToArray();
            if (data.Count() == 0) return null;

            var dt = new DataTable();
            foreach (var key in ((IDictionary<string, object>)data[0]).Keys)
            {
                dt.Columns.Add(key);
            }
            foreach (var d in data)
            {
                dt.Rows.Add(((IDictionary<string, object>)d).Values.ToArray());
            }
            return dt;
        }

        /// <summary>
        /// Metoda pomocniczna do testowania poprawności adresu Email
        /// </summary>
        /// <param name="emailaddress">string z adresem email</param>
        /// <returns>Zwraca true kiedy email poprawny, false w przeciwnym wypadku</returns>
        public static bool MailIsValid(string emailaddress)
        {
            try
            {
                MailAddress mail = new MailAddress(emailaddress);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Klasa pomocnicza z komunikatem o ograniczeniach implementacji systemu
        /// </summary>
        public static void DefaultMessage()
        {
            MessageBox.Show(MessageDictionary.DefaultMessage, MessageDictionary.DefaultMessageTitle);
        }
    }
}
