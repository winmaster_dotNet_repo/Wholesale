﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;
using HurtSys.Model;

namespace HurtSys.Controler
{
    /// <summary>
    /// Kontroler sterujący ekranem startowym
    /// </summary>
    public class HomeController
    {
        /// <summary>
        /// Lista zawierająca teksty newslettera
        /// </summary>
        public static List<string> newsletter= new List<string>();

        /// <summary>
        /// Funkcja ustawiająca tekst newslettera
        /// </summary>
        /// <param name="text">Kontener na newsletter</param>
        public static void SetNewsletterText(TextBox text)
        {
            LoadNewsletterInfo(newsletter);
            Random rnd = new Random();
            int number = rnd.Next(0,newsletter.Count);

            text.Text = newsletter[number];
        }

        /// <summary>
        /// Ładowanie danych newslettera
        /// </summary>
        /// <param name="list">Lista będąca kontenerem na dane</param>
        public static void LoadNewsletterInfo(List<string> list)
        {
            using (ResXResourceSet resxSet = new ResXResourceSet(BaseData.resxTexts))
            {
                foreach (DictionaryEntry entry in resxSet)
                {
                    list.Add((string) entry.Value);
                }
            }
        }

        /// <summary>
        /// Ustawianie listy potrzeb na ekranie startowym
        /// </summary>
        /// <param name="listOfNeeds">Kontener na listę potrzeb</param>
        public static void SetListOfNeedsHome(DataGridView listOfNeeds)
        {
            DbQueries.ListOfNeedsQuery();
            listOfNeeds.DataSource = null;
            listOfNeeds.DataSource = BaseController.DynamicToDataTable(BaseData.ListOfItemsOfNeeds);
        }

        /// <summary>
        /// Koloryzator do oznaczania stanów w liście potrzeb
        /// </summary>
        /// <param name="listOfNeeds">Kontener listy potrzeb</param>
        public static void ListOfNeedsColorizer(DataGridView listOfNeeds)
        {
            foreach (DataGridViewRow row in listOfNeeds.Rows)
            {
                if ( (Convert.ToInt32(row.Cells[1].Value)) > (Convert.ToInt32(row.Cells[3].Value)))
                {
                    row.DefaultCellStyle.BackColor = Color.Red;
                }
                else
                {
                    row.DefaultCellStyle.BackColor = Color.Yellow;
                }
            }
        }

        /// <summary>
        /// Funkcja odświerzająca listę potrzeb
        /// </summary>
        /// <param name="listOfNeeds">Kontener listy potrzeb</param>
        public static void DatagridViewListOfNeedsRefresher(DataGridView listOfNeeds)
        {
            listOfNeeds.DataSource = null;
            listOfNeeds.DataSource = BaseController.DynamicToDataTable(BaseData.ListOfItemsOfNeeds);
        }
    }
}