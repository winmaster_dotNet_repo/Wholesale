﻿using HurtSys.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using HurtSys.Resources;

namespace HurtSys.Controler
{
    /// <summary>
    /// Kontroler sterujący formularzem dostawy
    /// </summary>
    public class DeliveryController
    {
        /// <summary>
        /// Lista wiązana przedmiotów dostawy
        /// </summary>
        public static BindingList<DeliveryItem> DeliveryItems = new BindingList<DeliveryItem>();

        /// <summary>
        /// Ustawianie listy dostawy do kontenera DataGridView
        /// </summary>
        /// <param name="data">Kontener danych</param>
        public static void SetListDeliveryItemToDataGrid(DataGridView data)
        {
            data.DataSource = null;
            data.DataSource = DeliveryItems;
            data.Columns[0].HeaderText = MessageDictionary.ItemName_Header;
            data.Columns[1].HeaderText = MessageDictionary.ItemDeliver_Header;
            data.Columns[2].HeaderText = MessageDictionary.ItemAmount_Header;
            data.Columns[3].HeaderText = MessageDictionary.ItemPrice_Header;
            data.Columns[4].HeaderText = MessageDictionary.ItemPriceForAmount_Header;
            data.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            data.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            data.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        /// <summary>
        /// Metoda czyszcząca listę potrzeb o pozycje zawarte już w zamówieniu
        /// </summary>
        public static void CleanUpListOfNeeds()
        {
            if (DeliveryItems.Count != 0 && BaseData.ListOfItemsOfNeeds.Count != 0)
            {
                 DataTable dataOfNeeds = new DataTable();
                 dataOfNeeds = BaseController.DynamicToDataTable(BaseData.ListOfItemsOfNeeds);

                foreach (var deliveryItem in DeliveryItems)
                {
                    int index = 0;
                    foreach (DataRow datarow in dataOfNeeds.Rows)
                    {
                        if (deliveryItem.ItemName == datarow.ItemArray[0].ToString())
                        {
                            BaseData.ListOfItemsOfNeeds.RemoveAt(index);
                        }
                        index++;
                    }
                }
            }
        }

        /// <summary>
        /// Metoda usuwająca pozycję zamówienia w zamówieniu
        /// </summary>
        /// <param name="e">Argument eventu zawierający index wiersza do usunięcia</param>
        public static void DeleteDeliveryPosition(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(MessageDictionary.DeletePositionMessage, MessageDictionary.DeletePositionCaption, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    DeliveryItems.RemoveAt(e.RowIndex);
                }
            }
        }

        /// <summary>
        /// Metoda sprawdzająca czy użytkownik może opuścić edycję dostawy
        /// </summary>
        /// <returns>Zwraca true jeśli tak, przeciwnie false</returns>
        public static bool CheckIfUserWantToQuitEdition()
        {
            if (DeliveryItems.Count > 0)
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(MessageDictionary.CancelDelivery, MessageDictionary.DeliveryCaption, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    Application.Restart();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Metoda sprawdzająca czy użytkownik chce zamknąć aplikację podczas tworzenia dostawy
        /// </summary>
        public static void CheckIfUserWantToExitApp()
        {
            if (DeliveryItems.Count > 0)
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(MessageDictionary.CancelDelivery, MessageDictionary.ClosingApp,buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    Application.Exit();
                }
            }
            else Application.Exit();
        }

        /// <summary>
        /// Metoda czyszcząca listę zamówienia
        /// </summary>
        public static void CleanUpDeliveryItems()
        {
            DeliveryItems.Clear();
        }

        /// <summary>
        /// Metoda generująca i formatująca podsumowanie zamówienia
        /// </summary>
        /// <param name="summary">Kontener tekstowy na podsumowanie</param>
        public static void GenerateSummary(TextBox summary)
        {
            summary.Text = "";
            summary.AcceptsReturn = true;

            List<DeliveryItem> deliveryItemsSource =  new List<DeliveryItem>(DeliveryItems);

            //deliveryItemsSource  = deliveryItemsSource.OrderBy( i => i.ItemDeliver).ToList();
            var deliveryItemsByDelivers = deliveryItemsSource.GroupBy(a => a.ItemDeliver);

            decimal sumForDeliver;

            foreach (var deliver  in deliveryItemsByDelivers)
            {

                summary.Text += "\t\t----------------------------   "+deliver.Key.ToUpper() + "   ----------------------------\r\n\r\n\r\n";
                summary.Text += String.Format("{0,-25}{1,20}{2,20}{3,20}\r\n",MessageDictionary.ItemName_Header, MessageDictionary.ItemAmount_Header, MessageDictionary.ItemPrice_Header, MessageDictionary.ItemPriceForAmount_Header);
                summary.Text += "-------------------------------------------------------------------------------------------------------\r\n";
                sumForDeliver = 0;

                foreach (var d in deliver)
                {
                    sumForDeliver += d.PriceForAmount;
                    //summary.Text += d.ItemName + "\t\t\t" + d.Amount + "\t" + d.Price + "\t\t" +d.PriceForAmount + "\r\n";
                    summary.Text += String.Format("{0,-25}{1,20}{2,20:G}{3,20:G}\r\n", d.ItemName, d.Amount, d.Price , d.PriceForAmount);
                }
                summary.Text += String.Format("\r\n\r\n{0,70}{1,10:G}{2,5}\r\n",MessageDictionary.Sum,sumForDeliver, MessageDictionary.PLN);
                summary.Text += "*******************************************************************************************************\r\n\r\n";
            }
        }
    }
}
