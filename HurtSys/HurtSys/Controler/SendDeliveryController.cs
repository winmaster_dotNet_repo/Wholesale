﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using HurtSys.Resources;

namespace HurtSys.Controler
{
    /// <summary>
    /// Kontroler sterujący oknem wysyłania zamówienia
    /// </summary>
    public class SendDeliveryController
    {
        /// <summary>
        /// Sterowanie wysyłaniem zamówienia
        /// </summary>
        /// <param name="progressBar">Pasek stanu</param>
        /// <param name="picture">Ikona stanu</param>
        /// <returns>Zwraca true, jeśli wysłanie się powiodło, przeciwnie false</returns>
        public static bool SendDelivery(ProgressBar progressBar, PictureBox picture)
        {
            Random rand = new Random();
            int result = rand.Next(0, 10);
            bool tester = result % 5 == 0;
            
            switch (tester)
            {
                case false: // wysłanie powiodło się
                {
                    picture.Image = Properties.Resources.success;
                    MessageBox.Show(MessageDictionary.DeliverySuccess, MessageDictionary.Info);
                    DeliveryController.CleanUpDeliveryItems();
                    return true;
                }
                case true: //wysłanie się nie powiodło
                {
                    picture.Image = Properties.Resources.fail;
                    MessageBox.Show(MessageDictionary.Retry, MessageDictionary.DeliveryFail);
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Ustawianie backgroud workera, timera i progressbaru do sterowania wysyłaniem
        /// </summary>
        /// <param name="progressBarSendDelivery">progressBar wysyłki zamówienia</param>
        /// <param name="timer">Licznik czasu</param>
        /// <param name="backgroundWorker">background worker</param>
        public static void StartBackgroundWork(ProgressBar progressBarSendDelivery, Timer timer, BackgroundWorker backgroundWorker)
        {
            progressBarSendDelivery.Style = ProgressBarStyle.Continuous;
            progressBarSendDelivery.Maximum = 100;
            progressBarSendDelivery.Value = 0;
            timer.Enabled = true;

            backgroundWorker.RunWorkerAsync();
        }
    }
}
