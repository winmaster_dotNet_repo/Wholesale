﻿using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using HurtSys.Model;
using HurtSys.Resources;

namespace HurtSys.Controler
{
    /// <summary>
    /// Kontroler do obsługi formularza zmiany danych użytkownika
    /// </summary>
    public class AboutUserController
    {
        /// <summary>
        /// Ładowanie personalnych danych użytkownika
        /// </summary>
        /// <param name="name">Imię usera</param>
        /// <param name="surname">Nazwisko usera</param>
        /// <param name="email">Email usera</param>
        /// <param name="phone">Telefon usera</param>
        public static void LoadUserPersonalInformation(TextBox name, TextBox surname, TextBox email, TextBox phone)
        {
            name.Text = AccountManager.hurtSysUser.UsrName;
            surname.Text = AccountManager.hurtSysUser.UsrSurname;
            email.Text = AccountManager.hurtSysUser.UsrEmail;
            phone.Text = AccountManager.hurtSysUser.UsrPhone;
        }


        /// <summary>
        /// Walidacja poprawności zmiany hasła
        /// </summary>
        /// <param name="oldPass">tekst starego hasła</param>
        /// <param name="newPass">tekst nowego hasła</param>
        /// <param name="validateNewPass">tekst powtarzanego nowego hasła</param>
        /// <returns>Zwraca true jeśli walidacja pomyślna, przeciwnie false</returns>
        public static bool ValidatePasswordChange(TextBox oldPass, TextBox newPass, TextBox validateNewPass, bool showMessageBox = true)
        {
            string message = "";

            if (BaseController.sha256_hash(oldPass.Text) == AccountManager.hurtSysUser.Password)
            {
                if (newPass.Text.Length >= 6 && validateNewPass.Text.Length>= 6)
                {
                    if (validateNewPass.Text != newPass.Text)
                    {
                        message += MessageDictionary.PasswordsDontMatch;
                    }
                }
                else
                {
                    message += MessageDictionary.PasswordCharacters;
                }
            }
            else
            {
                message += MessageDictionary.CurrentPasswordDontMatch;
            }

            if (message != "")
            {
                if (showMessageBox) MessageBox.Show(message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Walidacja poprawności danych użytkownika
        /// </summary>
        /// <param name="name">Imię usera</param>
        /// <param name="surname">Nazwisko usera</param>
        /// <param name="email">Email usera</param>
        /// <param name="phone">Telefon usera</param>
        /// <returns>Zwraca true jeśli walidacja pomyślna, przeciwnie false</returns>
        public static bool ValidateUserPersonalInformation(TextBox name, TextBox surname, TextBox email, TextBox phone, bool showMessageBox = true)
        {
            bool changed = CheckIfUserPersonalDataChanged(name, surname, email, phone);
            string message="";

            name.ForeColor = Color.Black;
            surname.ForeColor = Color.Black;
            email.ForeColor = Color.Black;
            phone.ForeColor = Color.Black;

            //sprawdzanie czy spelniaja reguly
           
            if (name.Text != ""  && name.Text[0] == name.Text.ToUpper()[0])
            {
                if (surname.Text != "" && surname.Text[0] == surname.Text.ToUpper()[0])
                {
                    if (email.Text.Contains(MessageDictionary.HurtSysDomain) && BaseController.MailIsValid(email.Text))
                    {
                        if (phone.Text.Length == 9 && Regex.IsMatch(phone.Text, "^[0-9]+$"))
                        {
                            if (!changed)
                            {
                                message+= MessageDictionary.DataNotChanged;
                            }
                        }
                        else
                        {
                            message += MessageDictionary.PhoneNumberIncorrect;
                            phone.ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        message += MessageDictionary.Emailncorrect;
                        email.ForeColor = Color.Red;
                    }
                }
                else
                {
                    message += MessageDictionary.SurnameIncorrect;
                    surname.ForeColor = Color.Red;
                }
            }
            else
            {
                message += MessageDictionary.NameIncorrect;
                name.ForeColor= Color.Red;
            }

            if (message != "")
            {
                if (showMessageBox) MessageBox.Show(message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sprawdzanie czy dane użytkownika zostały zmienione
        /// </summary>
        /// <param name="name">Imię usera</param>
        /// <param name="surname">Nazwisko usera</param>
        /// <param name="email">Email usera</param>
        /// <param name="phone">Telefon usera</param>
        /// <returns>Zwraca true jeśli zmiany zaszły, przeciwnie false</returns>
        public static bool CheckIfUserPersonalDataChanged(TextBox name, TextBox surname, TextBox email, TextBox phone)
        {
            if (name.Text != AccountManager.hurtSysUser.UsrName) return true;
            if (surname.Text != AccountManager.hurtSysUser.UsrSurname) return true;
            if (email.Text != AccountManager.hurtSysUser.UsrEmail) return true;
            if (phone.Text != AccountManager.hurtSysUser.UsrPhone) return true;
            return false;
        }

        /// <summary>
        /// Sprawdzanie czy pola zmiany hasła zostały zmienione
        /// </summary>
        /// <param name="oldPass">tekst starego hasła</param>
        /// <param name="newPass">tekst nowego hasła</param>
        /// <param name="validateNewPass">tekst powtarzanego nowego hasła</param>
        /// <returns>Zwraca true jeśli zmiany zaszły, przeciwnie false</returns>
        public static bool CheckIfPasswordTextBoxChanged(TextBox oldPass, TextBox newPass, TextBox validateNewPass)
        {
            return oldPass.Text != "" || newPass.Text != "" || validateNewPass.Text != "";
        }
    }
}
