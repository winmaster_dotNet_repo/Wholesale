﻿using System.Windows.Forms;
using HurtSys.Model;

namespace HurtSys.Controler
{
    /// <summary>
    /// Kontroler do obsługi formularza dostawców
    /// </summary>
    public class DeliversController
    {
        /// <summary>
        /// Metoda ustawiająca zawartość kontenera z dostawcami
        /// </summary>
        /// <param name="delivers">Kontener na dostawców</param>
        public static void getAllDelivers(DataGridView delivers)
        {
            delivers.DataSource = null;
            delivers.DataSource = DbQueries.GetAllDeliversQuery();
        }
    }
}