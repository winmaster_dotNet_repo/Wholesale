﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using HurtSys.Model;

namespace HurtSys.Controler
{
    /// <summary>
    /// Kontroler obsługujący dodawanie produktu do dostawy
    /// </summary>
    public class AddProductToDeliveryController
    {
        private static bool _noResultInPriceSystem;
        private static bool _noResultInRateSystem;

        /// <summary>
        /// Ustawianie listy potrzeb do kontenera
        /// </summary>
        /// <param name="itemsOfNeeds">kontener na listę potrzeb</param>
        public static void SetItemsOfNeedsToDataGridView(DataGridView itemsOfNeeds)
        {
            itemsOfNeeds.DataSource = null;
            itemsOfNeeds.DataSource = BaseController.DynamicToDataTable(BaseData.ListOfItemsOfNeeds);
        }

        /// <summary>
        /// Funkcja sprawdzająca czy nie ma wyników wyszukiwania produktu
        /// </summary>
        /// <returns>Zwraca true jeśli brak wyników, przeciwnie false</returns>
        public static bool NoSearchResults()
        {
            return _noResultInPriceSystem && _noResultInRateSystem;
        }

        /// <summary>
        /// Wyszukiwanie produktu w systemie cenowym
        /// </summary>
        /// <param name="priceSystem">Kontener na wyniki</param>
        /// <param name="product">Nazwa produktu</param>
        public static void SearchItemInPriceSystem(DataGridView priceSystem, string product)
        {
            if (product == "") return;

            priceSystem.DataSource = null;
            List<dynamic> sourceList = new List<dynamic>();

            sourceList = DbQueries.SearchItemInPriceSystemQuery(product);
            
            if (sourceList.Count>0)
            {
                priceSystem.DataSource = BaseController.DynamicToDataTable(sourceList);
                priceSystem.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                _noResultInPriceSystem = false;
            }
            else
            {
                _noResultInPriceSystem = true;
            }
        }

        /// <summary>
        /// Wyszukiwanie produktu w systemie oceny dostawcy
        /// </summary>
        /// <param name="rateSystem">Kontener na wyniki</param>
        /// <param name="product">Nazwa produktu</param>
        public static void SearchItemInRateSystem(DataGridView rateSystem, string product)
        {
            if (product == "") return;
            rateSystem.DataSource = null;
            List<dynamic> sourceList = new List<dynamic>();

            sourceList = DbQueries.SearchItemInRateSystemQuery(product);

            if (sourceList.Count > 0)
            {
                rateSystem.DataSource = BaseController.DynamicToDataTable(sourceList);
                rateSystem.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                _noResultInRateSystem = false;
            }
            else
            {
                _noResultInRateSystem = true;
            }
        }

        /// <summary>
        /// Dodawanie produktu z systemu cenowego do zamówienia 
        /// </summary>
        /// <param name="data">Kontener danych</param>
        /// <param name="e">Przekazany z eventu kliknięcia wiersz kontenera</param>
        /// <param name="amount">Ilość produktu</param>
        /// <returns>Zwraca obiekt produktu dostawy</returns>
        public static DeliveryItem dataGridViewPriceSystem_AddProductToDelivery( DataGridView data, DataGridViewCellMouseEventArgs e, NumericUpDown amount)
        {
            int row = e.RowIndex;

            DeliveryItem deliveryItem=new DeliveryItem
            {
                Amount = (int)amount.Value,
                ItemName = (string)data[0, row].Value,
                ItemDeliver = (string) data[1, row].Value,
                Price =  Decimal.Parse((string)data[2, row].Value)
            };
            deliveryItem.PriceForAmount = deliveryItem.Price * deliveryItem.Amount;

            //MessageBox.Show(""+ deliveryItem.ItemName + " "+ deliveryItem.ItemDeliver + " il " + deliveryItem.Amount + " cena:  " + deliveryItem.Price + " suma " + deliveryItem.PriceForAmount);
           return deliveryItem;
        }

        /// <summary>
        /// Dodawanie produktu z systemu oceny dostawcy do zamówienia 
        /// </summary>
        /// <param name="data">Kontener danych</param>
        /// <param name="e">Przekazany z eventu kliknięcia wiersz kontenera</param>
        /// <param name="amount">Ilość produktu</param>
        /// <returns>Zwraca obiekt produktu dostawy</returns>
        public static DeliveryItem dataGridViewDeliversRateSystem_AddProductToDelivery(DataGridView data,DataGridViewCellMouseEventArgs e, NumericUpDown amount)
        {
            int row = e.RowIndex;

            DeliveryItem deliveryItem = new DeliveryItem
            {
                Amount = (int)amount.Value,
                ItemName = (string)data[0, row].Value,
                ItemDeliver = (string)data[1, row].Value,
            };

            decimal price = DbQueries.FindPriceForProduct(deliveryItem.ItemName, deliveryItem.ItemDeliver);

            deliveryItem.Price = price;
            deliveryItem.PriceForAmount = deliveryItem.Price * deliveryItem.Amount;

            //MessageBox.Show(""+ deliveryItem.ItemName + " "+ deliveryItem.ItemDeliver + " il " + deliveryItem.Amount + " cena:  " + deliveryItem.Price + " suma " + deliveryItem.PriceForAmount);
            return deliveryItem;
        }
    }

}
