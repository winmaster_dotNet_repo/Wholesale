# Projekt "Hurtownia" stworzony w ramach zajęć z przedmiotu "Projektowanie Oprogramowania"

## Jest to implementacja wycinka systemu informatycznego hurtowni produktów przemysłowych. Umożliwia pracownikowi działu zaopatrzenia kontrolę stanu towarów, tworzenie zamówień, podglądanie listy potrzeb, edycję swoich danych osobowych w tym hasła, a także podgląd listy dostawców.


### Implementacja systemu została wykonana w technologii .Net, w projekcie typu Windows Forms Application.
### Zastosowano wzorzec architektoniczny MVC (Model View Controller) oraz wzorzec projektowy Singleton.
### Baza danych została postawiona na serwerze w chmurze obliczeniowej MS Azure z wykorzystaniem języka T-SQL.
### W projekcie wykorzystano System ORM (Object-Relational Mapping) Dapper oraz pomocniczo Entity Framework.
### Testy jednostkowe zostały napisane z wykorzystaniem biblioteki NUnit framework.

## Ekran logowania:
![](screens/1.png)

## Główny widok:
![](screens/2.png)

## Zmiana danych użytkownika:
![](screens/3.png)

## Dodawanie produktów do zamówienia:
![](screens/4.png)

## Podsumowanie zamówienia:
![](screens/5.png)

## Symulacja wysyłania zamówienia do dostawcy:
![](screens/6.png)
